# Flappy Learning
Um algoritmo de aprendizado de máquina que aprende a jogar Flappy Bird utilizando Unity ML Agents.

## Comportamentos
Os comportamentos se encontram na pasta [Assets/_Project/Scripts](https://gitlab.com/paulohalves/flappy-learning/-/tree/master/Assets/_Project/Scripts). Os agentes comtém o nome &lt;ID&gt;AIBird.cs, onde &lt;ID&gt; implica os modelos analisados: um conjunto de observações (inputs) e suas recompensas/punições (fitness).
