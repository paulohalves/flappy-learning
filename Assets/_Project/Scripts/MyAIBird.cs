﻿using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;

[RequireComponent(typeof(Rigidbody2D))]
public class MyAIBird : BaseAIBird
{
    //Amalgama
    //Inputs: Horizontal distance from gate, vertical distance from upper pipe and vertical distance from bottom pipe
    //Fitness: Reward total time, reward gates passed and penalize collision

    [SerializeField] private Transform _eyes = null;
    [SerializeField] private LayerMask _gatesLayer = default;
    [SerializeField] private float _jumpDistance = 2.5f;

    private Rigidbody2D _rigidbody2D;
    private Gate _nextGate;

    private Vector2 DistanceFromGate
    {
        get
        {
            if (_nextGate == null)
            {
                return Vector2.zero;
            }

            return _nextGate.transform.position - transform.position;
        }
    }

    private Vector2 DistanceFromUpperGatePart
    {
        get
        {
            if (_nextGate == null)
            {
                return Vector2.zero;
            }

            return _nextGate.UpperPosition - transform.position;
        }
    }

    private Vector2 DistanceFromBottomGatePart
    {
        get
        {
            if (_nextGate == null)
            {
                return Vector2.zero;
            }

            return _nextGate.BottomPosition - transform.position;
        }
    }

    public Rigidbody2D Rigidbody2D
    {
        get
        {
            if (_rigidbody2D == null)
            {
                _rigidbody2D = GetComponent<Rigidbody2D>();
            }
            return _rigidbody2D;
        }
    }

    protected override void UpdateBehavior()
    {
        AddReward(Time.deltaTime);
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        GetNewGate();
        sensor.AddObservation(DistanceFromGate.x);
        sensor.AddObservation(DistanceFromUpperGatePart.y);
        sensor.AddObservation(DistanceFromBottomGatePart.y);
    }

    public override void Initialize()
    {
        base.Initialize();
        OnCollision += HandleOnCollision;
    }

    private void HandleOnCollision(BaseAIBird bird, string tag)
    {
        AddReward(-10);
    }

    public override void Heuristic(float[] actionsOut)
    {
        actionsOut[0] = 0;

        if (HeuristicInput)
        {
            actionsOut[0] = 1;
        }

        HeuristicInput = false;
    }

    public override void OnEpisodeBegin()
    {
        transform.position = Vector3.zero;
        Rigidbody2D.velocity = Vector2.zero;
    }

    public override void OnActionReceived(float[] vectorAction)
    {
        if (Mathf.FloorToInt(vectorAction[0]) == 1)
        {
            Jump();
        }
    }

    public void Jump()
    {
        Rigidbody2D.velocity = Vector2.up * _jumpDistance;
    }

    public void GetNewGate()
    {
        RaycastHit2D hit = Physics2D.Raycast(_eyes.position, _eyes.right, 100, _gatesLayer);

        if (hit)
        {
            _nextGate = hit.collider.GetComponentInParent<Gate>();
        }
    }

    public override void HandlePassedGate()
    {
        AddReward(10);
    }
}
