﻿using UnityEngine;
using Unity.MLAgents;

public abstract class BaseAIBird : Agent
{
    public delegate void CollisionHandler(BaseAIBird bird, string tag);
    public delegate float BirdHorizontalSpeedHandler();
    public event CollisionHandler OnCollision;

    private bool _heuristicInput = false;
    private BirdHorizontalSpeedHandler m_OnGetHorizontalSpeed;

    protected bool HeuristicInput { get => _heuristicInput; set => _heuristicInput = value; }
    public BirdHorizontalSpeedHandler OnGetHorizontalSpeed 
    { 
        get => m_OnGetHorizontalSpeed; 
        set => m_OnGetHorizontalSpeed = value; 
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            HeuristicInput = true;
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            HeuristicInput = false;
        }

        UpdateBehavior();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Wall" || collision.collider.tag == "BorderWall")
        {
            OnCollision?.Invoke(this, collision.collider.tag);
        }
    }

    protected abstract void UpdateBehavior();
    public abstract void HandlePassedGate();
}
