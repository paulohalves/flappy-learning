﻿using UnityEngine;

public class Gate : MonoBehaviour
{
    public delegate void ReachBorderHandler(Gate gate);
    public delegate void BirdScoreHandler(Gate gate, BaseAIBird bird);
    public event BirdScoreHandler OnBirdScore;
    public event ReachBorderHandler OnReachBorder;

    [SerializeField] private Transform _upperPart = null;
    [SerializeField] private Transform _bottomPart = null;

    private float _border;

    public Vector3 UpperPosition
    {
        get
        {
            return _upperPart.position;
        }
    }

    public Vector3 BottomPosition
    {
        get
        {
            return _bottomPart.position;
        }
    }

    private void Update()
    {
        if (transform.position.x < _border)
        {
            OnReachBorder?.Invoke(this);
        }
    }

    public void Setup(float border)
    {
        _border = border;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        BaseAIBird bird = collision.GetComponent<BaseAIBird>();
        if (bird)
        {
            OnBirdScore?.Invoke(this, bird);
        }
    }
}
