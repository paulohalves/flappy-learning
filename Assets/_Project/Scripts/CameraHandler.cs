﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHandler : MonoBehaviour
{
    [SerializeField] private float _forwardVelocity = 2.5f;

    private void FixedUpdate()
    {
        transform.position += Vector3.right * _forwardVelocity * Time.deltaTime;
    }
}
