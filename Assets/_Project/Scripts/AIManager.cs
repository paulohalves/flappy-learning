﻿using System.Collections.Generic;
using UnityEngine;

public class AIManager : MonoBehaviour
{
    public delegate void EpisodeHandler();
    public event EpisodeHandler OnEndEpisode;

    [SerializeField] private Object _birdPrefab = default;
    [SerializeField] private int _birdsCount = 1;
    [SerializeField] private bool _spawnBirds = false;

    private List<BaseAIBird> _spawnedBirds = new List<BaseAIBird>();

    private int _birdsRemaining;
    private BaseAIBird.BirdHorizontalSpeedHandler m_OnGetHorizontalSpeed;

    public BaseAIBird.BirdHorizontalSpeedHandler OnGetHorizontalSpeed
    {
        get => m_OnGetHorizontalSpeed;
        set => m_OnGetHorizontalSpeed = value;
    }

    private void Awake()
    {
        if (_spawnBirds)
        {
            for (int i = 0; i < _birdsCount; i++)
            {
                GameObject birdObj = Instantiate(_birdPrefab) as GameObject;
                BaseAIBird bird = birdObj.GetComponent<BaseAIBird>();
                bird.OnCollision += HandleOnBirdCollision;
                bird.OnGetHorizontalSpeed = OnGetHorizontalVelocity;
                _spawnedBirds.Add(bird);
            }
        }
        else
        {
            _spawnedBirds.AddRange(FindObjectsOfType<BaseAIBird>());
            _birdsCount = _spawnedBirds.Count;

            for (int i = 0; i < _birdsCount; i++)
            {
                _spawnedBirds[i].OnCollision += HandleOnBirdCollision;
                _spawnedBirds[i].OnGetHorizontalSpeed = OnGetHorizontalVelocity;
            }
        }

        _birdsRemaining = _birdsCount;
    }

    private float OnGetHorizontalVelocity()
    {
        if (OnGetHorizontalSpeed != null)
        {
            return OnGetHorizontalSpeed.Invoke();
        }

        return 1.0f;
    }

    private void HandleOnBirdCollision(BaseAIBird bird, string tag)
    {
        _birdsRemaining--;
        if (_birdsRemaining == 0)
        {
            for (int i = 0; i < _birdsCount; i++)
            {
                _spawnedBirds[i].EndEpisode();
            }
            _birdsRemaining = _birdsCount;
            OnEndEpisode?.Invoke();
        }
    }
}
