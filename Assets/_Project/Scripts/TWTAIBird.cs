﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;

[RequireComponent(typeof(Rigidbody2D))]
public class TWTAIBird : BaseAIBird
{
    //Tech With Tim: https://www.youtube.com/watch?v=OGHA-elMrxI
    //Inputs: Distance from upper and bottom pipes and bird height
    //Fitness: Reward total time and penalize for collisions

    [SerializeField] private Transform _eyes = null;
    [SerializeField] private LayerMask _gatesLayer = default;
    [SerializeField] private float _jumpDistance = 2.5f;

    private Rigidbody2D _rigidbody2D;
    private Gate _nextGate;

    private Vector2 UpperGatePart
    {
        get
        {
            if (_nextGate == null)
            {
                return Vector2.zero;
            }

            return _nextGate.UpperPosition;
        }
    }

    private Vector2 BottomGatePart
    {
        get
        {
            if (_nextGate == null)
            {
                return Vector2.zero;
            }

            return _nextGate.BottomPosition;
        }
    }

    public Rigidbody2D Rigidbody2D 
    {
        get
        {
            if (_rigidbody2D == null)
            {
                _rigidbody2D = GetComponent<Rigidbody2D>();
            }
            return _rigidbody2D;
        }
    }

    protected override void UpdateBehavior()
    {
        AddReward(Time.deltaTime);
    }

    public override void Initialize()
    {
        base.Initialize();
        OnCollision += HandleOnCollision;
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        base.CollectObservations(sensor);

        GetNewGate();
        sensor.AddObservation(Vector3.Distance(transform.position, UpperGatePart));
        sensor.AddObservation(Vector3.Distance(transform.position, BottomGatePart));
        sensor.AddObservation(transform.position.y);
    }

    private void HandleOnCollision(BaseAIBird bird, string tag)
    {
        if (tag == "Wall")
        {
            AddReward(-1);
        }
        else if (tag == "BorderWall")
        {
            AddReward(-5);
        }
    }

    public override void Heuristic(float[] actionsOut)
    {
        actionsOut[0] = 0;

        if (HeuristicInput)
        {
            actionsOut[0] = 1;
        }

        HeuristicInput = false;
    }

    public override void OnEpisodeBegin()
    {
        base.OnEpisodeBegin();

        transform.position = Vector3.zero;
        Rigidbody2D.velocity = Vector2.zero;
    }

    public override void OnActionReceived(float[] vectorAction)
    {
        base.OnActionReceived(vectorAction);

        if (Mathf.FloorToInt(vectorAction[0]) == 1)
        {
            Jump();
        }
    }

    protected void Jump()
    {
        Rigidbody2D.velocity = Vector2.up * _jumpDistance;
    }

    protected void GetNewGate()
    {
        RaycastHit2D hit = Physics2D.Raycast(_eyes.position, _eyes.right, 100, _gatesLayer);

        if (hit)
        {
            _nextGate = hit.collider.GetComponentInParent<Gate>();
        }
    }

    public override void HandlePassedGate()
    {
        GetNewGate();
        //AddReward(10);
    }
}
