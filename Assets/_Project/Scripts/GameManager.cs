﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GatesManager _gatesManager = default;
    [SerializeField] private AIManager _aIManager = default;
    [SerializeField] private HUDManager _hUDManager = default;
    [SerializeField] private float _timeScale = 1;

    private void Awake()
    {
        _gatesManager.OnBirdScore += HandleOnBirdScore;
        _aIManager.OnEndEpisode += HandleOnEndEpisode;
        _aIManager.OnGetHorizontalSpeed += () => _gatesManager.ForwardVelocity;
        Time.timeScale = _timeScale;
    }

    private void HandleOnEndEpisode()
    {
        _gatesManager.ResetGates();
        _hUDManager.Score = 0;
    }

    private void HandleOnBirdScore(BaseAIBird bird)
    {
        bird.HandlePassedGate();
        _hUDManager.Score++;
    }
}
