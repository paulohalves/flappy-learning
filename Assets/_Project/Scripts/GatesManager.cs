﻿using UnityEngine;

public class GatesManager : MonoBehaviour
{
    public delegate void BirdScoreHandler(BaseAIBird bird);
    public event BirdScoreHandler OnBirdScore;

    [SerializeField] private Transform _gatesAnchor = null;
    [SerializeField] private Transform _border = null;
    [SerializeField] private Gate[] _gates = null;
    [SerializeField] private Vector2 _heightInterval = default;
    [SerializeField] private float _horizontalInterval = 5.0f;
    [SerializeField] private float _initialXPosition = 5.0f;
    [SerializeField] private float _forwardVelocity = 2.5f;

    private float _lastPos;

    public float ForwardVelocity { get => _forwardVelocity; private set => _forwardVelocity = value; }

    private void Start()
    {
        SetupInitialGates();
    }

    private void FixedUpdate()
    {
        _gatesAnchor.position += -Vector3.right * ForwardVelocity * Time.deltaTime;
    }

    public void SetupInitialGates()
    {
        _gatesAnchor.transform.localPosition = Vector2.zero;
        _lastPos = _initialXPosition;
        for (int i = 0; i < _gates.Length; i++)
        {
            float height = Random.Range(_heightInterval.x, _heightInterval.y);

            _lastPos += _horizontalInterval;
            _gates[i].transform.localPosition = new Vector2(_lastPos, height);

            _gates[i].Setup(_border.position.x);
            _gates[i].OnBirdScore += HandleOnBirdScore;
            _gates[i].OnReachBorder += HandleOnReachBorder;
        }
    }

    public void ResetGates()
    {
        _gatesAnchor.transform.localPosition = Vector2.zero;
        _lastPos = _initialXPosition;
        for (int i = 0; i < _gates.Length; i++)
        {
            float height = Random.Range(_heightInterval.x, _heightInterval.y);

            _lastPos += _horizontalInterval;
            _gates[i].transform.localPosition = new Vector2(_lastPos, height);

            _gates[i].Setup(_border.position.x);
        }
    }

    private void HandleOnReachBorder(Gate gate)
    {
        float height = Random.Range(_heightInterval.x, _heightInterval.y);
        _lastPos += _horizontalInterval;
        gate.transform.localPosition = new Vector2(_lastPos, height);
    }

    private void HandleOnBirdScore(Gate gate, BaseAIBird bird)
    {
        OnBirdScore?.Invoke(bird);
    }
}
